
# Inversion - Better version control on your Mac

A new app that displays the status of your Git repo in a beautiful, usable and distraction-free interface. Inversion makes your local version control less painful with simple means.

To get started, open Inversion and select one of your Git repos to track. Inversion will display its status, changed files and history automatically. You should go ahead and select all the repos you have locally, as Inversion will automatically hide inactive repos out of the way.



## Git

As of now, Inversion **does not manipulate your repos**. It only reads your status, so you should be safe using test versions.



## Feedback and help

Please share any feedback, bug reports and feature suggestions via [Twitter](https://twitter.com/Eiskis) or [email](mailto:eiskis@gmail.com). We'll have Google Groups forums or something set up in the near future.

Inversion is developed by Jerry Jäppinen.

- [eiskis.net](http://eiskis.net/)
- [eiskis@gmail.com](mailto:eiskis@gmail.com)
- [@Eiskis](https://twitter.com/Eiskis)
